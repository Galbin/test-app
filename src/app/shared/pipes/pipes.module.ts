import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';

import { CapitalizePipe } from './capitalize-pipe';


const pipes: any = [
  CapitalizePipe
];

@NgModule({
  declarations: pipes,
  exports: pipes,
  imports: [IonicModule]
})

export class PipesModule {
}
