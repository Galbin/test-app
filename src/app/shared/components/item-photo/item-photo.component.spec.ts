import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ItemPhotoComponent } from './item-photo.component';
import { PipesModule } from '../../pipes/pipes.module';

describe('ItemPhotoComponent', () => {
  let component: ItemPhotoComponent;
  let fixture: ComponentFixture<ItemPhotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ItemPhotoComponent],
      imports: [IonicModule.forRoot(), PipesModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ItemPhotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
