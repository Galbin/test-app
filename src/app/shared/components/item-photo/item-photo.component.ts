import { Component, OnInit, Input } from '@angular/core';
import { Photo } from '../../models';

@Component({
  selector: 'app-item-photo',
  templateUrl: './item-photo.component.html',
  styleUrls: ['./item-photo.component.scss'],
})
export class ItemPhotoComponent implements OnInit {

  @Input()
  photo: Photo;
  constructor() { }

  ngOnInit() { }

  imageLoad() {
    this.photo.load = true;
  }
}
