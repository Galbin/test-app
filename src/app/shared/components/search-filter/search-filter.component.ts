import { Photo } from './../../models/api.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.scss'],
})
export class SearchFilterComponent {
  array: Array<Photo> = [];

  getItems(
    valorABuscar: string,
    arrayDatos: Photo[],
    arrayNombresCampos: string[]
  ) {
    this.array = arrayDatos;
    if (valorABuscar && valorABuscar.trim().length) {
      this.array = this.array.filter(item => {
        return this.buscar(item, arrayNombresCampos, valorABuscar);
      });
    }
    return this.array;
  }

  private buscar(
    item: object,
    arrayNombresCampos: string[],
    valorABuscar: string
  ) {
    let encontrado = false;
    arrayNombresCampos.forEach(nombreCampo => {
      if (
        item[nombreCampo] &&
        item[nombreCampo].toLowerCase().indexOf(valorABuscar.toLowerCase()) > -1
      ) {
        encontrado = true;
      }
    });
    return encontrado;
  }

}

