import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ComponentsModule } from './components/components.module';
import { PipesModule } from './pipes/pipes.module';
import { AlertService } from './services/alert';
import { ApiService } from './services/api/api.service';
import { InjectorService } from './services/injector';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ComponentsModule,
    PipesModule
  ],
  providers: [
    AlertService,
    ApiService,
    InjectorService
  ]
})

export class SharedModule { }
