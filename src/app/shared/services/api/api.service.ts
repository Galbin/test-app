import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Photo } from '../../models';


@Injectable()
export class ApiService {
  public photosJson: Array<Photo>;
  public photo: Photo;
  public chars: string = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  public numberchars: number = 15;
  public constructor() {
  }

  public getPhotos(): Observable<Array<Photo>> {
    this.photosJson = [];
    for (let index = 0; index < 4000; index++) {
      this.photo = { id: '' + index, photo: 'https://picsum.photos/id/' + index + '/500/500', text: this.randomString(this.numberchars, this.chars), load: false };
      this.photosJson.push(this.photo);
    }
    return of<Array<Photo>>(this.photosJson);
  }

  randomString(length, chars) {
    let result = '';
    for (let i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
  }

}
