import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { HomePageRoutingModule } from './home-routing.module';
import { ItemPhotoComponent } from 'src/app/shared/components/item-photo/item-photo.component';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { SearchFilterComponent } from 'src/app/shared/components/search-filter/search-filter.component';
import { ComponentsModule } from 'src/app/shared/components/components.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    PipesModule,
    ComponentsModule
  ],
  declarations: [HomePage],
  providers: [SearchFilterComponent]
})
export class HomePageModule { }
