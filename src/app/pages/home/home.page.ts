import { IonSearchbar, IonInfiniteScroll, IonVirtualScroll, LoadingController } from '@ionic/angular';
import { Component, ChangeDetectorRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Photo, Messages } from 'src/app/shared/models';
import { SearchFilterComponent } from 'src/app/shared/components/search-filter/search-filter.component';
import { AlertService } from 'src/app/shared/services/alert';
import { ApiService } from 'src/app/shared/services/api/api.service';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage {
  @ViewChild('mySearchbar') searchbar: IonSearchbar;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonVirtualScroll) virtualScroll: IonVirtualScroll;
  public photo: Photo;
  public photosJson: Array<Photo>;
  public photosJsonFiltro: Array<Photo>;

  public pageLimit = 10;
  public loaded = false;
  public loading = false;

  constructor(private filter: SearchFilterComponent,
    private cd: ChangeDetectorRef,
    private alertService: AlertService,
    private apiService: ApiService,
    private loadingCtrl: LoadingController) { }


  ngOnInit() {
    this.getPhotos();
  }

  getMoreElements() {
    this.loading = true;
    if (this.photosJson.length < 4000) {
      this.pageLimit = this.photosJson.length + this.pageLimit;
      for (let index = this.photosJson.length; index < this.pageLimit; index++) {
        this.photosJson.push(this.photosJsonFiltro[index]);
      }
    }
    if (this.virtualScroll)
      this.virtualScroll.checkEnd();
    if (this.infiniteScroll)
      this.infiniteScroll.complete();
    this.loading = false;
  }

  getItemsFilter(ev: any) {
    if (ev.type === 'ionClear') {
      this.pageLimit = 10;
      this.photosJson = this.photosJsonFiltro.slice(0, 10);
    } else {
      if (this.photosJsonFiltro)
        this.photosJson = this.filter.getItems(ev.target.value, this.photosJsonFiltro, ['id', 'text']).slice(0, 15);
    }
    this.cd.detectChanges();
  }

  doInfinite(event) {
    this.loaded = false;
    if (!this.loading)
      if (this.searchbar.value === '')
        this.getMoreElements();
      else {
        if (this.virtualScroll)
          this.virtualScroll.checkEnd();
        if (this.infiniteScroll)
          this.infiniteScroll.complete();
      }
  }


  private async getPhotos(): Promise<void> {
    await this.loadingCtrl.create({}).then((loading) => {
      loading.present();
      setTimeout(async () => {
        try {
          this.photosJsonFiltro = await this.apiService.getPhotos().toPromise();
          this.photosJson = this.photosJsonFiltro.slice(0, 10);
        } catch (err) {
          this.alertService.showOkAlert(Messages.photosApiError);
        } finally {
          await loading.dismiss();
        }
      }, 2000);
    });


  }
}
