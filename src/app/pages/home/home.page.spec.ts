
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, LoadingController } from '@ionic/angular';
import { HomePage } from './home.page';
import { PipesModule } from 'src/app/shared/pipes/pipes.module';
import { SearchFilterComponent } from 'src/app/shared/components/search-filter/search-filter.component';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { AlertService } from 'src/app/shared/services/alert';


describe('HomePage', () => {
  let app;
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HomePage],
      imports: [IonicModule.forRoot(), PipesModule],
      providers: [
        { provide: SearchFilterComponent },
        { provide: ApiService },
        { provide: AlertService },
        { provide: LoadingController }

      ]
    }).compileComponents();

    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
    app = fixture.debugElement.componentInstance;

  }));

  afterEach(() => {
    fixture.destroy();
    app = null;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Funcion: getItemsFilter', () => {
    const var2 = 'text';
    const array = [{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }];
    const var1 = 'id';
    const ev = { target: { value: 'Af' }, type: 'evento' };
    app.getItemsFilter(ev);
    expect([{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }]).toEqual([{ id: 0, photo: 'https://picsum.photos/id/0/500/500', text: 'Afvevhjjjjhkjhkj', load: false }]);
  });



});
